#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bintree.h"
int count = 0;
void insert(char *key, char *pw, node **leaf)
{
    int compString = 0;
    
    if (*leaf != NULL)
    {
    compString = strcmp(key, (*leaf)->data);
    }
    
    
    if (*leaf == NULL)
    {   
        *leaf = (node *) malloc(sizeof(node));
        strcpy((*leaf)->data, key);
        
        strcpy((*leaf)->plain, pw);
        /* initialize the children to null */
        (*leaf)->left = NULL;    
        (*leaf)->right = NULL;  
    }
    
    if(compString < 0)
    {
        insert (key, pw, &((*leaf)->left) );
    }
    if(compString > 0)
    {
        insert (key, pw, &((*leaf)->right) );
    }
    
}

void print(node *leaf)
{
	if (leaf == NULL) return;
	if (leaf->left != NULL) print(leaf->left);
	printf("%s\n", leaf->data);
	if (leaf->right != NULL) print(leaf->right);
}

// Modify so the key is the hash to search for
node *search(char *key, node *leaf)
{
    int compString;
    compString = strcmp(key, leaf->data);
      if (leaf != NULL)
      {
          if (compString==0){
             return leaf;
          } 
          else if (compString < 0){
             return search(key, leaf->left);
          }
          else{
             return search(key, leaf->right);
          }
      }
      else return NULL;
}

