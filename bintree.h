// TODO: modify the struct so it holds both the plaintext
// word and the hash.
typedef struct node {
	char data[33];
	char plain[40];
	struct node *left;
	struct node *right;
} node;

void insert(char *key, char *pw, node **leaf);

void print(node *leaf);

node *search(char *key, node *leaf);
