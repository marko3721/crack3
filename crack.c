#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"
#include "bintree.h"

const int PASS_LEN=40;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


// Read in the hash file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **read_hashes(char *filename)
{
    FILE *fp = fopen(filename,"r"); // read mode
    
    //Allocate array of 50 char pointers (strings)
    int size = 60;
    char **pwds = (char **)malloc(size * sizeof(char *));
    char str[40];
    int i = 0;
 
    if( fp == NULL )
    {
       printf("Error while opening the file.\n");
       exit(1);
    }
 
    while(fscanf(fp, "%s\n", str) != EOF)
    {  
       // If array is full, make it bigger
        if (i == size)
        {
            size = size + 50;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else
            {
                printf("Realloc failed\n");
                exit(1);
            }
        }
        char *newstr = (char *)malloc((strlen(str)+1) * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        i++;
    }
  //  printf("%d\n", i);
   i++;
   pwds[i] = NULL;
   fclose(fp);
   return pwds;
}

// Read in the dictionary file and return the tree.
// Each node should contain both the hash and the
// plaintext word.
node *read_dict(char *filename)
{
    
    node *tree = NULL;    
    
    FILE *fp = fopen(filename,"r");
    
    if( fp == NULL )
    {
       printf("Error while opening the file.\n");
       exit(1);
    }
    int countDict = 0;
    char str[40];
    while(fscanf(fp, "%[^\n]%*c", str) != EOF)
    {
        char *test1 = md5(str, strlen(str));
        insert(test1, str, &tree);
        countDict++;
        free(test1);
    }
    fclose(fp);
    return tree;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);
    
    // Read the dictionary file into a binary tree
    node *dict = read_dict(argv[2]);
    
    // For each hash, search for it in the binary tree.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    int countHashes = 0;
    while(hashes[countHashes] != NULL)
    {
       countHashes++;
    }
    for( int i = 0; i < countHashes; i++)
    {
        node *res;
        res = search(hashes[i], dict);
        if (res)
        printf("%s = %s\n", res->data, res->plain);
    }
    //free(hashes);
}
